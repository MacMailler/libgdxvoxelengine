package st.rhapsody.voxelengine.test.terrain.block;

import st.rhapsody.voxelengine.lib.terrain.block.Block;

/**
 * Created by nicklaslof on 19/01/15.
 */
public class TreeTrunkBlock extends Block {
    protected TreeTrunkBlock(byte id, String textureRegionTop, String textureRegionBottom, String textureRegionSides) {
        super(id, textureRegionTop, textureRegionBottom, textureRegionSides);
    }
}

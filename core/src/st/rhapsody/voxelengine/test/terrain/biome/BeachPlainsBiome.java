package st.rhapsody.voxelengine.test.terrain.biome;

/**
 * Created by nicklas on 6/18/14.
 */
public class BeachPlainsBiome extends PlainsBiome{

    @Override
    public boolean hasSandBeach() {
        return true;
    }

}

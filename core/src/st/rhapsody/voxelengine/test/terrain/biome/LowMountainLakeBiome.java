package st.rhapsody.voxelengine.test.terrain.biome;

/**
 * Created by nicklas on 6/19/14.
 */
public class LowMountainLakeBiome extends LowMountainBiome{

    @Override
    public double getAmmountOfWater() {
        return 12;
    }
}
